// Author: Miriam Lagergren
// Lesson task 1.2 - Drawing a square

#include<iostream>

int main() {
    float width;
    std::cout << "Enter no of rows and columns: "<<std::endl; 
    std::cin >> width; 
    
    // Loop ensuring input is non-negative and a whole number
    while (width < 0 || (int) width != width) {
    std::cout << "You have entered an invalid number"<<std::endl; 
    std::cin >> width; 
    }

    // Drawing the square
    for (int height = 1; height <= width ; height++){
        // Top and bottom rows needs more stars
        if (height == 1 || height == width){
            for (int i = 0; i < width; i++) {
                // Tiny rectangles requires less stars
                if (width < 3) {
                    std::cout << "*";
                } else {
                std::cout << "**";
                }
            }
            std::cout << std::endl;
        }
        // else statement draws out the middle of the rectangle
        else {
            std::cout << "*";
            for (int i = 1; i < width; i++) {
            std::cout << "  ";
            }
            std::cout << "*";
            std::cout << std::endl;
        }       
    }
    std::cout << "Press any key to continue...";
 return 0;
}